/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `config/404.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on routes, check out:
 * http://links.sailsjs.org/docs/config/routes
 */

module.exports.routes = {


  // Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, etc. depending on your
  // default view engine) your home page.
  //
  // (Alternatively, remove this and add an `index.html` file in your `assets` directory)
  '/': {
    view: 'homepage'
  },
  '/login': {
    controller: 'User',
    action: 'login'
  },
  '/logout':{
    controller: 'Session',
    action: 'destroy'
  },
  '/admin': {
    view: 'admin/index'
  },
  '/api/major/findByFaculty/:id': {
    controller: 'Major',
    action: 'findByFaculty'
  },
  '/api/user/find/id/:id': {
    controller: 'User',
    action: 'findByUserId'
  },
  '/api/user': {
    controller: 'User',
    action: 'getCurrentUser'
  },
  '/api/class/todayclass': {
    controller: 'Class',
    action: 'todayclass'
  },
  '/api/class/getClasses': {
    controller: 'Class',
    action: 'getClasses'
  },
  '/api/course/getCourses': {
    controller: 'Course',
    action: 'getCourses'
  },
  '/api/course/getAssignments': {
    controller: 'Course',
    action: 'getAssignments'
  },
  '/api/event/getEvents': {
    controller: 'Event',
    action: 'getEvents'
  },
  '/api/forum/getTopics': {
    controller: 'Forum',
    action: 'getTopics'
  },
  '/api/forum/getComments': {
    controller: 'Forum',
    action: 'getComments'
  },
  '/api/course/getTopics': {
    controller: 'Course',
    action: 'getTopics'
  },
  '/api/course/getComments': {
    controller: 'Course',
    action: 'getComments'
  },
  '/api/course/getMaterials': {
    controller: 'Course',
    action: 'getMaterials'
  },
  '/api/class/getMaterials': {
    controller: 'Class',
    action: 'getMaterials'
  },
  '/api/usage/topic': {
    controller: 'Forum_access',
    action: 'addTopic'
  },
  '/api/usage/comment': {
    controller: 'Comment_access',
    action: 'comment'
  },
  '/api/usage/material': {
    controller: 'User_access_material',
    action: 'accessMaterial'
  },
  '/api/usage/note': {
    controller: 'User_take_note',
    action: 'takeNote'
  },
  '/api/usage/photo': {
    controller: 'User_take_photo',
    action: 'takePhoto'
  },
  '/api/usage/voice': {
    controller: 'User_record_voice',
    action: 'recordVoice'
  },
  '/api/usage/dictionary': {
    controller: 'Dictionary_history',
    action: 'addHistory'
  }

  // Custom routes here...


  // If a request to a URL doesn't match any of the custom routes above,
  // it is matched against Sails route blueprints.  See `config/blueprints.js`
  // for configuration options and examples.

};
