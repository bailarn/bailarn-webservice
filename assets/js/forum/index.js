$(document).ready(function() {
	
	$('.deleteButton').click(function() {
				var button = $(this);
				var filename = button.prop('name');
				var eventId = button.prop('value');
				bootbox.confirm('Are you sure you want to delete ' + filename + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						console.log(eventId);
						$.ajax({
							type: 'delete',
							url: '/forum/delete?id=' + eventId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + eventId + ']').css("color", "lightgrey");
						});
					}
				});

			});
	$('.deleteButtonC').click(function() {
				var button = $(this);
				var filename = button.prop('name');
				var eventId = button.prop('value');
				bootbox.confirm('Are you sure you want to delete ' + filename + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						console.log(eventId);
						$.ajax({
							type: 'delete',
							url: '/forum/deleteComment?id=' + eventId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + eventId + ']').css("color", "lightgrey");
						});
					}
				});

			});

	$(".forumm").click(function(e){ // Click to only happen on announce links
	     e.preventDefault();
	     $("#course").val($(this).data('id'));
	     $('#forumModal').modal('show');
	   });

	$(".comme").click(function(e){ // Change page to comments page
	     
		//var dataid = $(this).data('id');
	     	//document.location.href = "../../../comment?"+ dataid;
		var data = $(this).data('id');
		/*var options = {
		*		url: '/forum/comment?id=' + data,
		*		type: 'GET',
		*		success: function(responseText, statusText, xhr, $form) {
		*			$('#uploadResultModal div.modal-body').html("Opening Comments.");
		*			$('#uploadResultModal').modal('show');
		*		}
		*	};
		*	$(this).ajaxSubmit(options);
			$(this).clearForm();*/
			e.preventDefault();
			//$("#forum").val($(this).data('id'));
			document.location.href = '../../../forum/comment?id=' + data;
	   });

	$('.addForumForm').submit(function(e) {
			var data = new FormData(this);
			var options = {
				url: '/forum/topicAdd',
				type: 'POST',
				success: function(responseText, statusText, xhr, $form) {
					$('#uploadResultModal div.modal-body').html("New forum has been added.");
					$('#uploadResultModal').modal('show');
				}
			};
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			$('#forumModal').modal('close');
			e.preventDefault();
		});
		
	$('.addCommentForm').submit(function(e) {
			var data = new FormData(this);
			var options = {
				url: '/forum/commentAdd',
				type: 'POST',
				success: function(responseText, statusText, xhr, $form) {
					$('#uploadResultModal div.modal-body').html("New comment has been added.");
					$('#uploadResultModal').modal('show');
				}
			};
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			$('#commentModal').modal('close');
			e.preventDefault();
		});
		
});
