$(document).ready(function() {
	
	$('.deleteButton').click(function() {
				var button = $(this);
				var filename = button.prop('name');
				var userId = button.prop('value');
				bootbox.confirm('Are you sure you want to delete ' + filename + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						//console.log(eventId);
						$.ajax({
							type: 'delete',
							url: '/master/delete?id=' + userId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + userId + ']').css("color", "lightgrey");
						});
					}
				});

			});
});


