$(document).ready(function() {

	$('.deleteButton').click(function() {
				var button = $(this);
				var filename = button.prop('name');
				var assignmentId = button.prop('value');
				var courseId = $('li.active').prop('id');
				bootbox.confirm('Are you sure you want to delete ' + filename + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						console.log(assignmentId);
						$.ajax({
							type: 'delete',
							url: '/assignment/delete?id=' + assignmentId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + assignmentId + ']').css("color", "lightgrey");
							getStudents(courseId);
						});
					}
				});

			});

	var overwriteFiles = [];
	var param = '?';
	$('input[name=assignment]').on('change', function() {
		overwriteFiles = [];
		var files = $(this)[0].files;
		var callbackCount = 0;
		for (var i = 0; i < files.length; i++) {
			var file = files[i];
			isNotDuplicated(file, function(err) {
				if (err) {
					bootbox.confirm(err + ' is duplicated. Do you want to overwrite the previous file?', function(result) {
						callbackCount++;
						if (result) {
							overwriteFiles.push(err);
							//console.log(overwriteFiles);
						}
						if (callbackCount == files.length) {
							console.log(overwriteFiles);
						}
					});
				} else {
					callbackCount++;
				}
			});
		}
	});

	function isNotDuplicated(file, callback) {
		var filename = file.name;
		$.ajax({
			type: 'get',
			url: '/assignment/duplicate?name=' + filename
		})
			.done(function(result) {
				console.log(result);
				callback(null);
			})
			.fail(function(result) {
				console.log(result.responseJSON);
				callback(filename);
			});
	}

	$('form')
		.submit(function(e) {
			var data = new FormData(this);
			var param = '?';
			overwriteFiles.forEach(function(obj) {
				param += 'overwrite=' + obj + '&';
			});
			var progress = $(this).children().children('.progress');
			var progressBar = $(this).children().children().children('.progress-bar')
			var cancelButton = $(this).children().children().children('button');
			var options = {
				url: '/assignment/upload' + param,
				type: 'POST',
				beforeSubmit: function() {
					progressBar.css('width', '0%');
					progress.show();
					cancelButton.show();
				},
				uploadProgress: function(event, position, total, percentComplete) {
					if (percentComplete == 100) {
						progressBar.css('width', percentComplete + '%').html('Processing...');
						progress.hide('slow');
						cancelButton.hide();
					} else {
						progressBar.css('width', percentComplete + '%').html(percentComplete + '%');
					}
				},
				beforeSend: function(xhr) {
					cancelButton.click(xhr.abort);
				},
				success: function(responseText, statusText, xhr, $form) {
					var result = '';
					responseText.forEach(function(obj) {
						result += obj.file + ' - ' + obj.result + '<br>';
					});
					$('#uploadResultModal div.modal-body').html(result);
					$('#uploadResultModal').modal('show');
				}
			}
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			e.preventDefault();
		});

});
