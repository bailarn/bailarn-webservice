$(document).ready(function() {
	
	$('.deleteButton').click(function() {
				var button = $(this);
				var filename = button.prop('name');
				var eventId = button.prop('value');
				bootbox.confirm('Are you sure you want to delete ' + filename + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						console.log(eventId);
						$.ajax({
							type: 'delete',
							url: '/event/delete?id=' + eventId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + eventId + ']').css("color", "lightgrey");
						});
					}
				});

			});

	$('.addEventForm').submit(function(e) {
			var data = new FormData(this);
			var options = {
				url: '/event/add',
				type: 'POST',
				success: function(responseText, statusText, xhr, $form) {
					$('#uploadResultModal div.modal-body').html("Event has been added.");
					$('#uploadResultModal').modal('show');
				}
			};
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			e.preventDefault();
		});
		
});
