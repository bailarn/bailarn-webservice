$(document).ready(function() {
	$('#university').on('load change', function() {
		var optionSelected = $("option:selected", this);
		$.ajax({
			type: 'get',
			url: '/university/' + optionSelected.val()
		}).success(function(res) {
			console.log(res);
			$('#faculty').html('');
			res.faculties.forEach(function(entry) {
				$('#faculty').append('<option value="' + entry.id + '">' + entry.name + '</option>');
			});
		});
	});
	$('.addUserForm').submit(function(e) {
			var data = new FormData(this);
			var options = {
				url: '/user/add',
				type: 'POST',
				success: function(responseText, statusText, xhr, $form) {
					$('#uploadResultModal div.modal-body').html("New Personel has been added.");
					$('#uploadResultModal').modal('show');
				}
			};
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			e.preventDefault();
		});
});
