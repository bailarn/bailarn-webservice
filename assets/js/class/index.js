$(document).ready(function() {
	$('.deleteButton').click(function() {
				var button = $(this);
				var filename = button.prop('name');
				var eventId = button.prop('value');
				bootbox.confirm('Are you sure you want to delete Cancel and Make up of' + filename + '?', function(result) {
					if (result) {
						button.addClass("disabled");
						console.log(eventId);
						$.ajax({
							type: 'delete',
							url: '/class/delete?id=' + eventId
						}).done(function(response) {
							console.log('Finish');
							console.log(response);
							$('tr[id=' + eventId + ']').css("color", "lightgrey");
						});
					}
				});

			});

	$('.addClassForm').submit(function(e) {
		var courseId = $(this).children().children('[name=course]').prop('value')
		var data = new FormData(this);
		var options = {
			url: '/class/add',
			type: 'POST',
			success: function(responseText, statusText, xhr, $form) {
				$('#uploadResultModal div.modal-body').html(responseText);
				$('#uploadResultModal').modal('show');
				getStudents(courseId);
			}
		};
		$(this).ajaxSubmit(options);
		$(this).clearForm();
		e.preventDefault();
	});

	$('.uploadForm')
		.submit(function(e) {
			var courseId = $(this).children().children('[name=course]').prop('value');
			console.log(courseId);
			var data = new FormData(this);
			var progress = $(this).children().children('.progress');
			var progressBar = $(this).children().children().children('.progress-bar')
			var cancelButton = $(this).children().children().children('button');
			var options = {
				url: '/class/upload',
				type: 'POST',
				beforeSubmit: function() {
					progressBar.css('width', '0%');
					progress.show();
					cancelButton.show();
				},
				uploadProgress: function(event, position, total, percentComplete) {
					if (percentComplete == 100) {
						progressBar.css('width', percentComplete + '%').html('Processing...');
						progress.hide('slow');
						cancelButton.hide();
					} else {
						progressBar.css('width', percentComplete + '%').html(percentComplete + '%');
					}
				},
				beforeSend: function(xhr) {
					cancelButton.click(xhr.abort);
				},
				success: function(responseText, statusText, xhr, $form) {
					$('#uploadResultModal div.modal-body').html("Students has been enrolled.");
					$('#uploadResultModal').modal('show');
					getStudents(courseId);
				}
			}
			$(this).ajaxSubmit(options);
			$(this).clearForm();
			e.preventDefault();
		});

});
