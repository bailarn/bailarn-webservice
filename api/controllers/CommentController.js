/**
 * CommentController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {

	index: function(req, res){
		  Topic.findOne().populateAll().exec(function(err, top) {
				var enrolledComment = [];
				for (var i = 0; i < top.comment_by.length; i++) {
					enrolledComment.push(top.comment_by[i].id);
				}
				console.log(enrolledComment);
				Comment.find({topic: enrolledComment}).populateAll().exec(function(err, comments) {
						/////////////////
						res.view({
							forums: top.comment_by, 
							comments: comments
						});
						/////////////////
				});
			});
		},

	commentAdd: function(req, res){
			Comment.create({
				topic_id: req.param('forum'),
				user_name: req.user.firstname,
			  	user_id: req.user.user_id,
				message: req.param('message'),

			}).exec(function(err, comment) {
			  if (err) {
			    console.log(err);
			  }else {
			    res.json(comment);
		
			    Topic.find(req.param('forum')).exec(function(err, topics) {
					topics.forEach(function(topic) {
						topic.comment_by.add(comment);
						topic.save(function(err) {
							if (err) {
							console.log(err);
							}
						});
					});
				});
			  }
			});
		
		},
	delete: function(req, res) {
		var forumId = req.param('id');
		Comment.findOne(forumId).exec(function(err, comment) {
			if (err) {
				return res.send(err);
			} else {
				comment.destroy();
				comment.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
};
