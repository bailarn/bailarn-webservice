/**
 * Comment_accessController
 *
 * @description :: Server-side logic for managing User_take_notes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	comment: function(req,res){
		var topic = req.param('topic_id');
		var username = req.param('user_name');
		var userid = req.param('user_id');
		var message = req.param('message');

		Comment.create({
			topic_id: topic,
			user_name: username,
		  	user_id: userid,
			message: message
		}).exec(function(err, comment) {
			if(err){
				console.log(err);
				res.send(403);
			}
			else{
			res.json(comment);
			    Topic.find(topic).exec(function(err, topics) {
					topics.forEach(function(topic) {
						topic.comment_by.add(comment);
						topic.save(function(err) {
							if (err) {
							console.log(err);
							}
						});
					});
				});
				res.send(200);
			}
		});
	}
};
