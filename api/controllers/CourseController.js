/**
 * CourseController
 *
 * @description :: Server-side logic for managing Courses
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	// @param User's ObjectId
	// @return Courses that the user enrolled
	'getCourses': function(req, res) { //53ba20dae88af3d515a2e2ca
		var access_token = req.param('access_token');
		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token) {
			if (err) res.json(err);
			else {
				User.findOne(token.userId)
					.populateAll()
					.exec(function(err, user) {
						if (err) {
							res.json(err);
						} else {
							var enrolledCourses = [];
							for (var i = 0; i < user.study_courses.length; i++) {
								enrolledCourses.push(user.study_courses[i].id);
							}
							Course.find(enrolledCourses).populateAll().exec(function(err, c) {
								res.json(c);
							});
						}
					});
			}
		});
	},
	getMaterials: function(req, res) {
		var courseId = req.param('course_id');
		Material.find({
			course: courseId
		}).populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Material not found.');
			else
				res.json(m);
		});
	},
	getAssignments: function(req, res) {
		var courseId = req.param('course_id');
		Assignment.find({
			course: courseId
		}).populateAll().exec(function(err, a) {
			if (err) res.send(404, 'Material not found.');
			else
				res.json(a);
		});
	},
	getTopics: function(req, res){
		var courseId = req.param('course_id');
		Topic.find({
			course: courseId
		}).populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Topic not found.');
			else
				res.json(m);
		});
	},
	
	getComments: function(req, res){
		var topicId = req.param('topic_id');
		Comment.find({
			topic_id: topicId
		}).populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Comment not found.');
			else
				res.json(m);
		});
	},
	index: function(req, res) {
	University.find().populateAll().exec(function(err, uni) {
		Faculty.find().populateAll().exec(function(err, fac) {
		User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Course.find().populateAll().exec(function(err, c) {
			Course.find(enrolledCourses).populateAll().exec(function(err, courses) {
				var students = [];
				for (var i = 0; i < courses.length; i++) {
					var course = courses[i];
					var studentsInThisCourse = [];
					for (var j = 0; j < course.study_by.length; j++) {
						var student = course.study_by[j];
						studentsInThisCourse.push(student);
					}
					students.push(studentsInThisCourse);
				}
				var instructor = "Instructor";
				User.find({type: instructor}).exec(function(err, u) {	
				for (var i = 0; i < u.length; i++) {
					var j=i;
				}
				User.find({type: "Admin"}).exec(function(err, u) {	
				for (var i = 0; i < u.length; i++) {
					var k = i;
						}
				res.view({
					universities: uni,
					faculties: fac,
					admin: user,
					allc: c,
					courses: courses,
					students: students,
					iN: j,
					aN: k
				});
				});
				});
			});
			});
		});
		});
	});
	},
	getStudents: function(req, res) {
		var courseId = req.param('id');
		Course.findOne(courseId).populate('study_by').exec(function(err, course) {
			if (err || !course) {
				return res.send(404, 'Course not found');
			} else {
				return res.json(course.study_by);
			}
		});
	},
	withdraw: function(req, res) {
		var studentId = req.param('id');
		var courseId = req.param('course');
		Course.findOne(courseId).exec(function(err, course) {
			if (err) {
				return res.send(err);
			} else {
				course.study_by.remove(studentId);
				course.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
	add: function(req, res) {
		var studentId = req.param('id');
		var courseId = req.param('course');
		var universityId = req.user.university;
		var facultyId = req.user.faculty;
		console.log(studentId + ' ' + courseId + ' ' + universityId + ' ' + facultyId);
		Course.findOne(courseId).exec(function(err, course) {
			if (err || !course) {
				return res.send('Course not found.');
			} else {
				User.findOne({
					user_id: studentId,
					university: universityId,
					faculty: facultyId
				}).exec(function(err, student) {
					if (err || !student) {
						res.send('Student not found')
					} else {
						course.study_by.add(student);
						course.save(function(err) {
							if (err) {
								res.send('Student is already enrolled');
							} else {
								res.send('Student has been enrolled.');
							}
						});
					}
				});
			}
		});
	},
	///////////////////////////////
	ecAdd: function(req, res){
		var instructorId = req.user.user_id;
		var courseId = req.param('newc');
		var universityId = req.user.university;
		var facultyId = req.user.faculty;
		console.log(courseId);
		User.find({
				user_id: instructorId,
				university: universityId,
				faculty: facultyId
		}).exec(function(err, instruc) {
		instruc.forEach(function(ins) {
			ins.teach_courses.add(courseId);
			ins.save(function(err) {
				if (err) {
				console.log(err);
				}
				});
			});
		});
	},	

	ncAdd: function(req, res){
		Course.create({
			university: req.user.university,
			faculty: req.user.faculty,
		  	course_code: req.param('course_code'),
			name: req.param('name'),
			credit: req.param('credit'),
			description: req.param('description'),
			semester: req.param('semester'),
			year: req.param('year')
		}).exec(function(err, course) {
		  if (err) {
		    console.log(err);
		  }else {
		    res.json(course);
		  }
		});
	},
	////////////////////////////
	upload: function(req, res) {
		University.findOne(req.user.university).exec(function(err, university) {
			Faculty.findOne(req.user.faculty).exec(function(err, faculty) {
				Course.findOne(req.param('course')).exec(function(err, course) {
					var d = new Date();
					var timestamp = d.getTime();
					var sessionID = req.sessionID;
					var uniqueFilename = timestamp + sessionID + '-';
					var directory = '/assets/course/';

					var receiver = blobAdapter().receive({
						dirname: sails.config.appPath + directory,
						saveAs: function(file) {
							var filename = file.filename,
								newName = uniqueFilename + filename;
							return newName;
						}
					});

					mkdirp(directory, function(err) {
						req.file('student_list')
							.upload(receiver, function onUploadComplete(err, uploadedFiles) {
								if (err) return res.send(err);
								else {
									try {
										var file = uploadedFiles[0];
										if (typeof require !== 'undefined') XLS = require('xlsjs');
										//Read file
										var workbook = XLS.readFile(sails.config.appPath + directory + uniqueFilename + file.filename);
										//Delete file
										fs.unlink(sails.config.appPath + directory + uniqueFilename + file.filename, function(err) {
											if (err) {
												return res.send(err);
											}

										});
										var sheetNames = workbook.SheetNames;
	
										for (var i = 0; i < sheetNames.length; i++) {
											var students = []; //studentId
											var sfn =[]; //student firstname
											var sln =[]; //student lastname
											var sg = []; //student gender
											var ss = []; //student section
											var sheetName = sheetNames[i];
											var sheet = workbook.Sheets[sheetName];
											var cellnumber;
											var section;
											if(sheetName=="Sec.1")section = 1;
											else if(sheetName=="Sec.2")section = 2;
											else if(sheetName=="Sec.3")section = 3;
											if (sheet.B8.v) { //Small Version students' ids start at B6
												cellnumber = 8; //console.log(sheet.B8.v);
											} else if (sheet.B6.v) { //Full Version students' ids start at B8
												cellnumber = 6; //console.log(sheet.B6.v);
											}
											try {
												do {
													var studentIdCell = 'B' + cellnumber;
													var studentfnCell = 'G' + cellnumber;
													var studentlnCell = 'H' + cellnumber;
													var studentgCell = 'F' + cellnumber;
													var studentId = sheet[studentIdCell].w;
													var fn = sheet[studentfnCell].w;
													var ln = sheet[studentlnCell].w;
													var g = sheet[studentgCell].w;
													if(g == "MR.")g = "Male";
													else if(g == "MISS")g = "Female";
													students.push(studentId);
													sfn.push(fn);
													sln.push(ln);
													sg.push(g);
													ss.push(section);
													cellnumber++;
												}
												while (true);
											} catch (err) {
												console.log(err);
											}
											//sections.push(students);
											for(var z = 0; z<students.length; z++){
											User.create({
												user_id: students[z],
												firstname: sfn[z],
												lastname: sln[z],
												gender: sg[z],
												section: ss[z],
												username: students[z],
												password: students[z],
												type: 'Student',
												university: req.user.university,
												faculty: req.user.faculty
												}).exec(function(err, event) {
													if (err) console.log(err);
													else
														res.json(event);
												});
											}
										}
									} catch (err) {
										res.send(err);
									}
									
									res.send(200);
								}
							});
					});
				});
			});
		});
	},

	////////////////////////////
	/*uploadExist: function(req, res) {
		University.findOne(req.user.university).exec(function(err, university) {
			Faculty.findOne(req.user.faculty).exec(function(err, faculty) {
				Course.findOne(req.param('course')).exec(function(err, course) {
					var d = new Date();
					var timestamp = d.getTime();
					var sessionID = req.sessionID;
					var uniqueFilename = timestamp + sessionID + '-';
					var directory = '/assets/course/';

					var receiver = blobAdapter().receive({
						dirname: sails.config.appPath + directory,
						saveAs: function(file) {
							var filename = file.filename,
								newName = uniqueFilename + filename;
							return newName;
						}
					});

					mkdirp(directory, function(err) {
						req.file('student_list')
							.upload(receiver, function onUploadComplete(err, uploadedFiles) {
								if (err) return res.send(err);
								else {
									try {
										var file = uploadedFiles[0];
										if (typeof require !== 'undefined') XLS = require('xlsjs');
										//Read file
										var workbook = XLS.readFile(sails.config.appPath + directory + uniqueFilename + file.filename);
										//Delete file
										fs.unlink(sails.config.appPath + directory + uniqueFilename + file.filename, function(err) {
											if (err) {
												return res.send(err);
											}

										});
										var sheetNames = workbook.SheetNames;
	
										for (var i = 0; i < sheetNames.length; i++) {
											var students = []; //studentId
											var sheetName = sheetNames[i];
											var sheet = workbook.Sheets[sheetName];
											var cellnumber;
											if (sheet.B8.v) { //Small Version students' ids start at B6
												cellnumber = 8; //console.log(sheet.B8.v);
											} else if (sheet.B6.v) { //Full Version students' ids start at B8
												cellnumber = 6; //console.log(sheet.B6.v);
											}
											try {
												do {
													var studentIdCell = 'B' + cellnumber;
													var studentId = sheet[studentIdCell].w;
													students.push(studentId);
													cellnumber++;
												}
												while (true);
											} catch (err) {
												console.log(err);
											}
											//sections.push(students);
											//Add if existed
											//console.log(students.length)
											for(var z=0; z<5; z++){
											User.findOne({
												university: university.id,
												faculty: faculty.id,
												user_id: students[z]
											}).exec(function(err, student) {
												console.log(student);
													course.study_by.add(student);
													course.save(function(err) {
														if (err) {
															console.log(err);
														}
													});
											});
											}
										}
									} catch (err) {
										res.send(err);
									}
									res.send(200);
								}
							});
					});
				});
			});
		});
	},*/

	addInstructor: function(req, res) {
	
	var userid = req.param('userid');
	if(userid < 10) userid = "0000" + userid;
	else if(userid < 100) userid = "000" + userid;
	else if(userid < 1000) userid = "00" + userid;
	else if(userid < 10000) userid = "0" + userid;
	
	User.create({
		user_id: "ib" + userid,
		firstname: req.param('fname'),
		lastname: req.param('lname'),
		gender: req.param('gender'),
		username: req.param('uname'),
		password:req.param('pass'),
		tel: req.param('tel'),
		birthday: req.param('bdate'),
		email: req.param('email'),
		type: 'Instructor',
		university: req.user.university,
		faculty: req.user.faculty
		}).exec(function(err, event) {
			if (err) console.log(err);
			else
				res.json(event);
		});
	},

	addAdmin: function(req, res) {	

	var userid = req.param(userid);
	if(userid < 10) userid = "0000" + userid;
	else if(userid < 100) userid = "000" + userid;
	else if(userid < 1000) userid = "00" + userid;
	else if(userid < 10000) userid = "0" + userid;
	
	User.create({
		user_id: "ab" + userid,
		firstname: req.param('fname'),
		lastname: req.param('lname'),
		gender: req.param('gender'),
		username: req.param('uname'),
		password:req.param('pass'),
		tel: req.param('tel'),
		birthday: req.param('bdate'),
		email: req.param('email'),
		type: 'Admin',
		university: req.param('university'),
		faculty: req.param('faculty')
		}).exec(function(err, event) {
			if (err) console.log(err);
			else
				res.json(event);
		});
	},

	addStudent: function(req, res) {
	
	User.create({
		user_id: req.param('id'),
		firstname: req.param('fname'),
		lastname: req.param('lname'),
		gender: req.param('gender'),
		section: req.param('section'),
		username: req.param('id'),
		password: req.param('id'),
		type: 'Student',
		university: req.user.university,
		faculty: req.user.faculty
		}).exec(function(err, event) {
			if (err) console.log(err);
			else
				res.json(event);
		});
	}

};
