/**
 * MasterController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
		index: function(req, res){
		var instructor = "Instructor"
		var admin = "Admin"
		  User.find({type: instructor}).populateAll().exec(function(err, u) {
			User.find({type: admin}).populateAll().exec(function(err, ua) {
				var typeOf = [];
				for (var i = 0; i < u.length; i++) {
					typeOf.push(u[i].id);
				}
				for (var i = 0; i < ua.length; i++) {
					typeOf.push(ua[i].id);
				}
				//console.log(typeOf);
				User.find(typeOf).populateAll().exec(function(err, users) {
				//console.log(users);
						/////////////////
						res.view({
							users: users
						});
						/////////////////
				});
			});
		  });
		},

		delete: function(req, res) {
		var userId = req.param('id');
		User.findOne(userId).exec(function(err, u) {
			if (err) {
				return res.send(err);
			} else {
				u.destroy();
				u.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
};
