/**
 * UniversityController
 *
 * @description :: Server-side logic for managing universities
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function(req, res) {
		University.find()
			.populate('faculties')
			.exec(function(err, universities) {
				if (err) {
					res.json(err);
				} else {
					var getFaculties = function(){
						Faculty.find().exec(function(err, faculties){
							return faculties;
						});
					}
					res.view({
						universities: universities,
						dumb : getFaculties
					});
				}
			});
	}
};