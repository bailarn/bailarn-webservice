/**
 * ForumController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	index: function(req, res){
	  User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Topic.find({course: enrolledCourses}).populateAll().exec(function(err, topic) {
				Comment.find().populateAll().exec(function(err, comments) {
					/////////////////
					res.view({courses: user.teach_courses,forums: topic,comments: comments});
					/////////////////
				});
			});
		});
	},
	
	delete: function(req, res) {
		var forumId = req.param('id');
		Topic.findOne(forumId).exec(function(err, topic) {
			if (err) {
				return res.send(err);
			} else {
				topic.destroy();
				topic.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
	
	topicAdd: function(req, res){
		Topic.create({
			course:req.param('course'),
			title: req.param('title'),
			description: req.param('description'),
		  	view: 0,
			user_name: req.user.firstname,
		  	user_id: req.user.user_id
		}).exec(function(err, topic) {
		  if (err) {
		    console.log(err);
		  }else {
		    res.json(topic);
		  }
		});
	},

	comment: function(req, res){
		var id = req.param('id')
		//console.log(id);
		  Topic.findOne(id).populateAll().exec(function(err, top) {
				//console.log(top.id);
				var enrolledComment = [];
				for (var i = 0; i < top.comment_by.length; i++) {
					enrolledComment.push(top.comment_by[i].id);
				}
				//console.log(enrolledComment);
				Comment.find({topic_id: enrolledComment}).populateAll().exec(function(err, comments) {
				console.log(comments);
						/////////////////
						res.view({
							topics: top,
							forums: top.comment_by, 
							comments: comments
						});
						/////////////////
				});
			});
		},

	commentAdd: function(req, res){
			Comment.create({
				topic_id: req.param('forum'),
				user_name: req.user.firstname,
			  	user_id: req.user.user_id,
				message: req.param('message'),

			}).exec(function(err, comment) {
			  if (err) {
			    console.log(err);
			  }else {
			    res.json(comment);
		
			    Topic.find(req.param('forum')).exec(function(err, topics) {
					topics.forEach(function(topic) {
						topic.comment_by.add(comment);
						topic.save(function(err) {
							if (err) {
							console.log(err);
							}
						});
					});
				});
			  }
			});
		
		},

		deleteComment: function(req, res) {
		var forumId = req.param('id');
		Comment.findOne(forumId).exec(function(err, comment) {
			if (err) {
				return res.send(err);
			} else {
				comment.destroy();
				comment.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},

	getTopics: function(req, res){
		Topic.find().populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Topic not found.');
			else
				res.json(m);
		});
	},
	
	getComments: function(req, res){
		Comment.find().populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Comment not found.');
			else
				res.json(m);
		});
	},
};
