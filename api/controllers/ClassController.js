/**
 * ClassController
 *
 * @description :: Server-side logic for managing Classes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	todayClass: function(req, res) {
		var access_token = req.param('access_token');
		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token) {
			User.findOne(token.userId).populateAll().exec(function(err, user) {
				var enrolledCourses = [];
				for (var i = 0; i < user.courses.length; i++) {
					enrolledCourses.push(user.courses[i].id);
				}
				var todayStart = new Date();
				todayStart.setHours(0);
				todayStart.setMinutes(0);
				todayStart.setSeconds(0);
				var todayEnd = new Date();
				todayEnd.setHours(23);
				todayEnd.setMinutes(59);
				todayEnd.setSeconds(59);
				Class.find({
					datetime_start: {
						'>': todayStart,
						'<': todayEnd
					},
					course: enrolledCourses,
					status : ['Normal','Makeup']
				}).populateAll().exec(function(err, c) {
					res.json(c);
				});
			});
		});

	},
	getClasses: function(req, res) {
		var course_id = req.param('id');
		Class.find({
			course: course_id
		}).populateAll().exec(function(err, m) {
			res.json(m);
		});
	},
	getMaterials: function(req, res) {
		var class_id = req.param('class_id');
		Material.find({
			class: class_id
		}).populateAll().exec(function(err, m) {
			res.json(m);
		});
	},
	index: function(req, res) {
		User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Cancelmakeup.find({
				course: enrolledCourses
			}).populateAll().exec(function(err, classes) {
				res.view({
					courses: user.teach_courses,
					classes: classes,
				});
			});
		});
	},
	withdraw: function(req, res) {
		var classId = req.param('id');
		var courseId = req.param('course');
		Course.findOne(courseId).exec(function(err, course) {
			if (err) {
				return res.send(err);
			} else {
				course.study_by.remove(classId);
				course.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
	delete: function(req, res) {
		var classId = req.param('id');
		Cancelmakeup.findOne(classId).exec(function(err, c) {
			if (err) {
				return res.send(err);
			} else {
				c.destroy();
				c.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},
	add: function(req, res) {
		var classId = req.param('id');
		var courseId = req.param('course');
		var cm = req.param('c_m');
		if(cm < 10) cm = '0' + cm;
		var ch = req.param('c_h');
		if(ch < 10) ch = '0' + ch;
		var mm = req.param('m_m');
		if(mm < 10) mm = '0' + mm;
		var mh = req.param('m_h');
		if(mh < 10) mh = '0' + mh;
		var canceltime = ch + ':' + cm;
		var makeuptime = mh + ':' + mm;
		var checkbox = req.param('yup');
		console.log(classId + ' ' + courseId);
		Course.findOne(courseId).exec(function(err, course) {
			if (err || !course) {
				return res.send('Course not found.');
			} else {
				Class.findOne({
					class_id: classId,
				}).exec(function(err, clas) {
					if (err || !clas) {
						res.send('Class not found')
					} else {
						Cancelmakeup.create({
							course:req.param('course'),
							instructor: req.user.firstname,
							datetime_cancel: req.param('date_cancel')+' '+canceltime,
						  	datetime_makeup: req.param('date_makeup')+' '+makeuptime,
						  	location: req.param('location'),
						  	section: req.param('section')
						}).exec(function(err, event) {
						  if (err) {
						    console.log(err);
						  }else {
						    res.json(event);
						  }
						});
					}
				});
			}
		});
		if(checkbox == "send"){
		Course.findOne(req.param('course')).exec(function(err, course) {
		Parse.Push.send({
		      channels: [ course.name, "Mets" ],
		      data: {
			alert: "Cancel & Make-up: " + course.course_code +" - " +course.name +" - has been annouced."
		      }
		    }, {
		      success: function() {
			// Push was successful
		      },
		      error: function(error) {
			// Handle error
			res.send(error,401);
		      }
		    });
		});
		}
	}
};
