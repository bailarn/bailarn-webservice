/**
 * MaterialController
 *
 * @description :: Server-side logic for managing materials
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	upload: function(req, res) {
		var overwriteFilenames = req.param('overwrite');
		console.log(overwriteFilenames);
		var checkbox = req.param('yup');
		University.findOne(req.user.university).exec(function(err, university) {
			Faculty.findOne(req.user.faculty).exec(function(err, faculty) {
				Course.findOne(req.param('course')).exec(function(err, course) {
					var d = new Date();
					var timestamp = d.getTime();
					var sessionID = req.sessionID;
					var uniqueFilename = timestamp + sessionID + '-';
					var universityFolderName = '/' + university.name.replace(/\s/g, "_");
					var facultyFolderName = '/' + faculty.name.replace(/\s/g, "_");
					var courseFolderName = '/' + course.name.replace(/\s/g, "_");
					var directory = '/assets/materials' + universityFolderName + facultyFolderName + courseFolderName + '/';

					var receiver = blobAdapter().receive({
						dirname: sails.config.appPath + directory,
						saveAs: function(file) {
							var filename = file.filename,
								newName = uniqueFilename + filename;
							return newName;
						}
					});

					function validateFile(file, callback) {
						Material.findOne({
							name: file.filename
						}).exec(function(err, material) {
							if (err) {
								console.log(err);
								return callback({
									file: file.filename,
									result: err
								});
							}
							var overwrite = false;
							var version = 1;
							if (material) {
								//Check if the material need to be overwritten
								
								if (overwriteFilenames != null && overwriteFilenames.indexOf(material.name) >= 0) {
									overwrite = true;
									version = material.version+1;
									material.destroy(function(err) {
										if (err) {
											callback({
												file: file.filename,
												result: err
											});
										} else {
											fs.unlink(sails.config.appPath + material.filepath, function(err) {
												if (err) {
													callback({
														file: file.filename,
														result: err
													});
												} else {
													return;
												}

											});
										}
									});
								}
							}

							if (!material || overwrite) {
								if (!overwrite)
									console.log(file.filename + ' is not duplicated');
								else
									console.log(file.filename + ' will be overwritten');
								
								Material.create({
									name: file.filename,
									filepath: directory + uniqueFilename + file.filename,
									course: req.param('course'),
									version: version,
									upload_by: req.user.id
								}).exec(function(err, material) {
									if (err) {
										console.log(err);
										callback({
											file: file.filename,
											result: err
										});
									} else {
										console.log(material.name + ' is uploaded.');
										callback({
											file: file.filename,
											result: 'success'
										});
									}
								});
							} else {
								console.log(file.filename + ' is duplicated');
								//console.log(materials);
								fs.unlink(sails.config.appPath + directory + uniqueFilename + file.filename, function(err) {
									if (err) {
										console.log(err);
										callback({
											file: file.filename,
											result: err
										});
									} else {
										console.log(file.filename + ' is deleted.');
										callback({
											file: file.filename,
											result: 'duplicate'
										});
									}

								});
							}
						});
					}

					var results = [];
					mkdirp(directory, function(err) {
						req.file('material')
							.upload(receiver, function onUploadComplete(err, uploadedFiles) {
								if (err) return res.json(err);
								else {

									uploadedFiles.forEach(function(file) {
										validateFile(file, function(result) {
											results.push(result);
											if (results.length == uploadedFiles.length) {
												res.send(200, results);
											}
										});
									});
								}
							});
					});
				});
			});
		});
		if(checkbox == "send"){
		Course.findOne(req.param('course')).exec(function(err, course) {
		Parse.Push.send({
		      channels: [ course.name , "Mets" ],
		      data: {
			alert: "Material: " +course.course_code +" - " +course.name +" - has been added."
		      }
		    }, {
		      success: function() {
			// Push was successful
		      },
		      error: function(error) {
			// Handle error
			res.send(error,401);
		      }
		    });
		});
		}
	},
	index: function(req, res) {
		User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Material.find({
				course: enrolledCourses
			}).populateAll().exec(function(err, materials) {
				res.view({
					courses: user.teach_courses,
					materials: materials
				});
			});
		});
	},
	download: function(req, res) {
		var fileId = req.param('id');
		Material.findOne(fileId).exec(function(err, material) {
			if (err) return res.json(err);
			if (!material) return res.send(404);
			var file = sails.config.appPath + material.filepath;
			var filename = material.name;
			var mimetype = mime.lookup(file);

			res.setHeader('Content-disposition', 'attachment; filename=' + filename);
			res.setHeader('Content-type', mimetype);

			var filestream = fs.createReadStream(file);
			filestream.pipe(res);
		});

	},
	delete: function(req, res) {
		if (req.route.method != 'delete') {
			return res.json('Wrong verb');
		}
		var materialId = req.param('id');

		function authenticateDeleteRequest(req, callback) {
			if (!req.user) {
				return callback('Unauthorized, not login', null);
			} else {
				User.findOne({
					id: req.user.id,
					type: ['Instructor', 'Admin']
				}).populateAll().exec(function(err, user) {
					if (err || !user) {
						return callback(err);
					} else {
						Material.findOne(materialId).populateAll().exec(function(err, material) {
							if (err) {
								return callback(err);
							} else if (!material) {
								return callback('Material not found');
							} else {
								for (var i = 0; i < user.teach_courses.length; i++) {
									var course = user.teach_courses[i];
									if (course.id == material.course.id) {
										return callback(null, material);
									}
								}
								return callback('Unauthorized, not enough priviledges');

							}
						});
					}
				});
			}

		}
		authenticateDeleteRequest(req, function(err, material) {
			if (err) {
				console.log(err);
				return res.send(403, err);
			} else {
				material.destroy(function(err) {
					if (err) {
						return res.json(err);
					} else {
						fs.unlink(sails.config.appPath + material.filepath, function(err) {
							if (err) {
								return res.json(err);
							} else {
								return res.json('Delete success');
							}

						});
					}
				});
			}
		});
	},
	getMaterials: function(req, res) {
		var courseId = req.param('id');
		Material.find({
			course: courseId
		}).populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Material not found.');
			else
				res.json(m);
		});
	},
	duplicate: function(req, res) {
		var filename = req.param('name');
		Material.find({
			name: filename
		}).exec(function(err, materials) {
			if (err) {
				return res.send(err);
			} else {
				if (materials.length > 0) {
					return res.json(403, filename + ' is duplicated.');
				} else {
					return res.json(200, filename + ' is not duplicated.');
				}
			}
		});
	}
};
