/**
 * Forum_accessController
 *
 * @description :: Server-side logic for managing User_take_photoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	addTopic: function(req,res){
		var course = req.param('course'); //course_id
		var title = req.param('title');
		var username = req.param('user_name');
		var userid = req.param('user_id');
		var description = req.param('description');
		Topic.create({
			course: course,
			title: title,
		  	view: 0,
			user_name: username,
		  	user_id: userid,
			description: description
		}).exec(function(err, topic) {
			if(err)
				res.send(403);
			else
				res.send(200);
		});
	}
};
