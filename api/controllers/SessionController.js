/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport');

module.exports = {
	new: function(req, res) {
		//console.log('Login request');
		passport.authenticate(
			'local',
			function(err, user, info) {
				//console.log('SessionController.js');
				//console.log(err);
				//console.log(user);
				if ((err) || (!user)) {
					res.redirect('/user/login');
					return;
				}
				// use passport to log in the user using a local method
				req.logIn(
					user,
					function(err) {
						if (err) {
							res.redirect('/user/login');
							return;
						}
						res.redirect('/');
						return;
					}
				);
			}
		)(req, res);
	},
	destroy: function(req,res) {
		req.logout();
		res.redirect('/');
	}
};