/**
 * AssignmentController
 *
 * @description :: Server-side logic for managing assignments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	upload: function(req, res) {
		var overwriteFilenames = req.param('overwrite');
		console.log(overwriteFilenames);
		var checkbox = req.param('yup');
		University.findOne(req.user.university).exec(function(err, university) {
			Faculty.findOne(req.user.faculty).exec(function(err, faculty) {
				Course.findOne(req.param('course')).exec(function(err, course) {
					var d = new Date();
					var timestamp = d.getTime();
					var sessionID = req.sessionID;
					var uniqueFilename = timestamp + sessionID + '-';
					var universityFolderName = '/' + university.name.replace(/\s/g, "_");
					var facultyFolderName = '/' + faculty.name.replace(/\s/g, "_");
					var courseFolderName = '/' + course.name.replace(/\s/g, "_");
					var directory = '/assets/assignments' + universityFolderName + facultyFolderName + courseFolderName + '/';

					var receiver = blobAdapter().receive({
						dirname: sails.config.appPath + directory,
						saveAs: function(file) {
							var filename = file.filename,
								newName = uniqueFilename + filename;
							return newName;
						}
					});

					function validateFile(file, callback) {
						Assignment.findOne({
							name: file.filename
						}).exec(function(err, assignment) {
							if (err) {
								console.log(err);
								return callback({
									file: file.filename,
									result: err
								});
							}
							var overwrite = false;
							var version = 1;
							if (assignment) {
								//Check if the assignment need to be overwritten
								
								if (overwriteFilenames != null && overwriteFilenames.indexOf(assignment.name) >= 0) {
									overwrite = true;
									version = assignment.version+1;
									assignment.destroy(function(err) {
										if (err) {
											callback({
												file: file.filename,
												result: err
											});
										} else {
											fs.unlink(sails.config.appPath + assignment.filepath, function(err) {
												if (err) {
													callback({
														file: file.filename,
														result: err
													});
												} else {
													return;
												}

											});
										}
									});
								}
							}

							if (!assignment || overwrite) {
								if (!overwrite)
									console.log(file.filename + ' is not duplicated');
								else
									console.log(file.filename + ' will be overwritten');
								var sm = req.param('s_m');
								if(sm < 10) sm = '0' + sm;
								var sh = req.param('s_h');
								if(sh < 10) sh = '0' + sh;
								var submittime = sh + ':' + sm;
								Event.create({
									course: req.param('course'),
									name: req.param('title'),
									description: req.param('description'),
									datetime_start: d,
									datetime_end: req.param('submit_date')+ ' '+ submittime,
									type: 'assignment',
									author: req.user.firstname
								}).exec(function(err, event){
									if(err){
										console.log(err);
									}
									else {
										res.json(event);
									}
								});
								Assignment.create({
									name: file.filename,
									title: req.param('title'),
									description: req.param('description'),
									submit_datetime: req.param('submit_date')+ ' ' + submittime,
									filepath: directory + uniqueFilename + file.filename,
									course: req.param('course'),
									version: version,
									upload_by: req.user.id
								}).exec(function(err, assignment) {
									if (err) {
										console.log(err);
										callback({
											file: file.filename,
											result: err
										});
									} else {
										console.log(assignment.name + ' is uploaded.');
										callback({
											file: file.filename,
											result: 'success'
										});
									}
								});
							} else {
								console.log(file.filename + ' is duplicated');
								//console.log(assignments);
								fs.unlink(sails.config.appPath + directory + uniqueFilename + file.filename, function(err) {
									if (err) {
										console.log(err);
										callback({
											file: file.filename,
											result: err
										});
									} else {
										console.log(file.filename + ' is deleted.');
										callback({
											file: file.filename,
											result: 'duplicate'
										});
									}

								});
							}
						});
					}

					var results = [];
					mkdirp(directory, function(err) {
						req.file('assignment')
							.upload(receiver, function onUploadComplete(err, uploadedFiles) {
								if (err) return res.json(err);
								else {

									uploadedFiles.forEach(function(file) {
										validateFile(file, function(result) {
											results.push(result);
											if (results.length == uploadedFiles.length) {
												res.send(200, results);
											}
										});
									});
								}
							});
					});
				});
			});
		});
		if(checkbox == "send"){
		Course.findOne(req.param('course')).exec(function(err, course) {
		Parse.Push.send({
		      channels: [ course.name, "Mets" ],
		      data: {
			alert: "Assignment: " +course.course_code +" - " +course.name +" - has been added."
		      }
		    }, {
		      success: function() {
			// Push was successful
		      },
		      error: function(error) {
			// Handle error
			res.send(error,401);
		      }
		    });
		});
		}
	},
	index: function(req, res) {
		User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Assignment.find({
				course: enrolledCourses
			}).populateAll().exec(function(err, assignments) {
				res.view({
					courses: user.teach_courses,
					assignments: assignments
				});
			});
		});
	},
	download: function(req, res) {
		var fileId = req.param('id');
		Assignment.findOne(fileId).exec(function(err, assignment) {
			if (err) return res.json(err);
			if (!assignment) return res.send(404);
			var file = sails.config.appPath + assignment.filepath;
			var filename = assignment.name;
			var mimetype = mime.lookup(file);

			res.setHeader('Content-disposition', 'attachment; filename=' + filename);
			res.setHeader('Content-type', mimetype);

			var filestream = fs.createReadStream(file);
			filestream.pipe(res);
		});

	},
	delete: function(req, res) {
		if (req.route.method != 'delete') {
			return res.json('Wrong verb');
		}
		var assignmentId = req.param('id');

		function authenticateDeleteRequest(req, callback) {
			if (!req.user) {
				return callback('Unauthorized, not login', null);
			} else {
				User.findOne({
					id: req.user.id,
					type: ['Instructor', 'Admin']
				}).populateAll().exec(function(err, user) {
					if (err || !user) {
						return callback(err);
					} else {
						Assignment.findOne(assignmentId).populateAll().exec(function(err, assignment) {
							if (err) {
								return callback(err);
							} else if (!assignment) {
								return callback('Assignment not found');
							} else {
								for (var i = 0; i < user.teach_courses.length; i++) {
									var course = user.teach_courses[i];
									if (course.id == assignment.course.id) {
										return callback(null, assignment);
									}
								}
								return callback('Unauthorized, not enough priviledges');

							}
						});
					}
				});
			}

		}
		authenticateDeleteRequest(req, function(err, assignment) {
			if (err) {
				console.log(err);
				return res.send(403, err);
			} else {
				assignment.destroy(function(err) {
					if (err) {
						return res.json(err);
					} else {
						fs.unlink(sails.config.appPath + assignment.filepath, function(err) {
							if (err) {
								return res.json(err);
							} else {
								return res.json('Delete success');
							}

						});
					}
				});
			}
		});
	},
	getAssignments: function(req, res) {
		var courseId = req.param('id');
		Assignment.find({
			course: courseId
		}).populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Assignment not found.');
			else
				res.json(m);
		});
	},
	duplicate: function(req, res) {
		var filename = req.param('name');
		Assignment.find({
			name: filename
		}).exec(function(err, assignments) {
			if (err) {
				return res.send(err);
			} else {
				if (assignments.length > 0) {
					return res.json(403, filename + ' is duplicated.');
				} else {
					return res.json(200, filename + ' is not duplicated.');
				}
			}
		});
	}
};
