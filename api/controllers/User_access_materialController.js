/**
 * User_access_materialController
 *
 * @description :: Server-side logic for managing User_access_materials
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	accessMaterial: function(req,res){
		var user = req.param('user');
		var material = req.param('material');

		User_access_material.create({
			user : user,
			material : material
		}).exec(function(err){
			if(err)
				res.send(403);
			else
				res.send(200);
		});
	}
};

