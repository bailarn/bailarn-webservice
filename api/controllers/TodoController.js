/**
 * TodoController
 *
 * @description :: Server-side logic for managing Todoes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	test: function(req, res) {
		var now = new Date();
		Todo.create({
			title: 'hey',
			due_datetime: now,
			type: 'official',
			creator: 2,
			course: 3
		}).exec(function(err, todo) {
			if (err) console.log(err);
			else
				res.json(todo);
		});
	}
};