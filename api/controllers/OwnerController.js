/**
 * OwnerController
 *
 * @description :: Server-side logic for managing owners
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	test: function(req, res) {
		Owner.findOne(2).exec(function(err, owner) {
			var pts = owner.pets;
			pts.push(3);
			Owner.update(2, {
				pets: pts
			}).exec(function(err, owner) {
				if (err) console.log(err);
				res.json(owner);
			});
		});
	}

};