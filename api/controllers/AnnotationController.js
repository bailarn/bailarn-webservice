/**
 * AnnotationController
 *
 * @description :: Server-side logic for managing annotations
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	getAnnotation : function(req,res){
		var materialId = req.param('id');
		var access_token = req.param('access_token');
		AccessToken.findOne({
			token: access_token
		}).exec(function(err, token){
			if(err || !token)
				return res.send(403, err);
			else{
				Annotation.findOne({
					user : token.userId,
					material : materialId
				}).exec(function(err, annotation){
					if(err || !annotation){
						return res.send(403, err);
					}
					else{
						return res.json(annotation);
					}
				});
			}
		});
	}
};

