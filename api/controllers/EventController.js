/**
 * EventController
 *
 * @description :: Server-side logic for managing events
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var fs = require('fs');
var mkdirp = require('mkdirp');
var path = require('path');
var mime = require('mime');
var blobAdapter = require('skipper-disk');

module.exports = {
	testcreate: function(req, res) {
		var d = new Date();
		Event.create({
			course: 2,
			name: 'Seminar',
			datetime_start: d,
			datetime_end: d+36000,
			description: 'at room IT321, please bring calculator',
			type: 'Seminar'
		}).exec(function(err, event) {
			if (err) console.log(err);
			else
				res.json(event);
		});
	},

	add: function(req, res){
		var sm = req.param('s_m');
		if(sm < 10) sm = '0' + sm;
		var sh = req.param('s_h');
		if(sh < 10) sh = '0' + sh;
		var em = req.param('e_m');
		if(em < 10) em = '0' + em;
		var eh = req.param('e_h');
		if(eh < 10) eh = '0' + eh;
		var starttime = sh + ':' + sm;
		var endtime = eh + ':' + em;

		var checkbox = req.param('yup');
		//console.log(checkbox);
		Event.create({
			course:req.param('course'),
			name: req.param('name'),
		  	datetime_start: req.param('date_start')+' '+starttime,
		  	datetime_end: req.param('date_end')+' '+endtime,
		  	description: req.param('description'),
			author: req.user.firstname,
		  	type: req.param('type')
		}).exec(function(err, event) {
		  if (err) {
		    console.log(err);
		  }else {
		    res.json(event);
		  }
		});
		if(checkbox == "send"){
		Course.findOne(req.param('course')).exec(function(err, course) {
		if(course.name != 'common'){
		Parse.Push.send({
		      channels: [ course.name , "Mets" ],
		      data: {
			alert: "Event: " + course.course_code +" - " +course.name +" - has been added."
		      }
		    }, {
		      success: function() {
			// Push was successful
		      },
		      error: function(error) {
			// Handle error
			res.send(error,401);
		      }
		    });
		}
		if(course.name == 'common'){
		Parse.Push.send({
		      channels: [ "Bailarn" , "Mets" ],
		      data: {
			alert: "Event: " + course.course_code +" - " +course.name +" - has been added."
		      }
		    }, {
		      success: function() {
			// Push was successful
		      },
		      error: function(error) {
			// Handle error
			res.send(error,401);
		      }
		    });
		}
		});
		}
	},

	getEvents: function(req, res){
		Event.find().populateAll().exec(function(err, m) {
			if (err) res.send(404, 'Event not found.');
			else
				res.json(m);
		});
	},
	delete: function(req, res) {
		var eventId = req.param('id');
		Event.findOne(eventId).exec(function(err, event) {
			if (err) {
				return res.send(err);
			} else {
				event.destroy();
				event.save(function(err) {
					if (err) {
						res.send(err);
					} else {
						res.send(200);
					}
				});
			}
		});
	},

	index: function(req, res){
	  User.findOne(req.user.id).populateAll().exec(function(err, user) {
			var enrolledCourses = [];
			for (var i = 0; i < user.teach_courses.length; i++) {
				enrolledCourses.push(user.teach_courses[i].id);
			}
			Event.find({
				course: enrolledCourses
			}).populateAll().exec(function(err, events) {
				res.view({
					courses: user.teach_courses,
					events: events
				});
			});
		});
}
};
