/**
 * Comment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		user_id: {
			type: 'string',
			required: true
		},
		user_name: {
			type: 'string',
			required: true
		},
		topic_id: {
			type: 'string',
			required: true
		},
		message: {
			type: 'string',
			required: true
		},
		comment_to: {
			collection: 'Topic',
			via : 'comment_by'
		}
	}
};
