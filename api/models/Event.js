/**
 * Event.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		course: {
			model: 'Course'
		},
		name: {
			type: 'string',
			required: true
		},
		datetime_start: {
			type: 'datetime',
			required: true
		},
		datetime_end: {
			type: 'datetime',
			required: true
		},
		author: {
			type: 'string'
		},
		description: {
			type: 'text'
		},
		type: {
			type: 'string',
			required: true
		}//Exam, Training, Seminar, Conference, Faculty_Activity, University_Activity
	}
};
