/**
 * University.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		name: {
			type: 'string',
			required: true
		},
		abbr_name: {
			type: 'string',
			required: true
		},
		address: {
			type: 'text'
		},
		tel: {
			type: 'string'
		},
		faculties: {
			collection: 'Faculty',
			via: 'university'
		}
	}
};