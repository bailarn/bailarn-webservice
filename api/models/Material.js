/**
 * Material.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		'class': {
			model: 'Class'
		},
		course:{
			model: 'Course'
		},
		name: {
			type: 'string'
		},
		version: {
			type: 'int'
		},
		upload_by: {
			model: 'User'
		},
		filepath: {
			type: 'string'
		},
		external_filepath: {
			type: 'string'
		}
	}
};