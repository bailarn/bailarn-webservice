/**
 * Class.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		course: {
			model: 'Course',
			required: true
		},
		datetime_cancel: {
			type: 'datetime',
			required: true
		},
		datetime_makeup: {
			type: 'datetime',
			required: true
		},
		location: {
			type: 'string',
			required: true
		},
		instructor: {
			type: 'string',
			required: true
		},
		section: {
			type: 'int'
		}
	}
};
