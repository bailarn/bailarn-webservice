/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		user_id: {
			type: 'string',
			required: true
		},
		firstname: {
			type: 'string',
			required: true
		},
		lastname: {
			type: 'string',
			required: true
		},
		gender: {
			type: 'string',
			required: true
		},
		year: {
			type: 'int'
		},
		section: {
			type: 'int'
		},
		major: {
			type: 'string'
		},
		username: {
			type: 'string',
			required: true
		},
		password: {
			type: 'string',
			required: true
		},
		email: {
			type: 'email'
		},
		tel: {
			type: 'string'
		},
		birthday: {
			type: 'date'
		},
		type: {
			type: 'string',
			required: true
		},
		university: {
			model: 'University',
			required: true
		},
		faculty: {
			model: 'Faculty',
			required: true
		},
		major: {
			model: 'Major'
		},
		study_courses: {
			collection: 'Course',
			via : 'study_by'
		},
		teach_courses: {
			collection: 'Course',
			via : 'teach_by'
		},
		todos: {
			collection: 'Todo',
			via: 'creator'
		},
		dictionary_histories: {
			collection: 'Dictionary_history',
			via: 'user'
		},
		access_materials: {
			collection: 'User_access_material',
			via: 'user'
		},
		toJSON: function() {
			var obj = this.toObject();
			delete obj.password;
			return obj;
		}
	},
	beforeCreate: function(values, next){
		require('bcrypt').hash(values.password, 10, function(err, encryptedPassword){
			if(err) return next(err);
			values.password = encryptedPassword;
			next();
		});
	}
};