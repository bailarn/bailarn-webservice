/**
 * Topic.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
	schema: true,
	attributes: {
		course:{
			model: 'Course'
		},
		user_id: {
			type: 'string',
			required: true
		},
		user_name: {
			type: 'string',
			required: true
		},
		title: {
			type: 'string',
			required: true
		},
		view: {
			type: 'int'
		},
		description: {
			type: 'string'
		},
		comment_by:{
			collection: 'Comment',
			via : 'comment_to',
			dominant : true
		}
	}
};
