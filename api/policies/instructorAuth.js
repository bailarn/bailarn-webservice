/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

module.exports = function(req, res, next) {
	if (req.isAuthenticated()) {
		if(req.user.type == "Instructor"){
			return next();
		}

		else if(req.isAuthenticated() && req.user.type == "Admin") {
		return next();
		}

		else if(req.isAuthenticated() && req.user.type == "Master") {
		return next();
		}

		else
			return res.forbidden('This page is not allowed for student.');
	} else {
		return res.redirect('/login');
	}
	// User is not allowed
	// (default res.forbidden() behavior can be overridden in `config/403.js`)
};
